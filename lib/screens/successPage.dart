import 'package:flutter/material.dart';

class SuccessPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SizedBox(
            child: Center(
              child: Text(
                "Thanks for Registration!!",
                style: TextStyle(
                  decoration: TextDecoration.none,
                  decorationColor: Colors.red,
                ),
              )
            ),
        ),
      ],
    );
    
  }
}
