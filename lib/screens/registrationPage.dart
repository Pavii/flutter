import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:myapp/screens/successPage.dart';
import 'package:myapp/users.dart';

class RegistrationPage extends StatefulWidget {
  RegistrationPage({Key key, this.register}) : super(key: key);
  final String register;
  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  final GlobalKey<FormState> _formkey = new GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController mobileController = TextEditingController();
  TextEditingController genderController = TextEditingController();
  TextEditingController dobController = TextEditingController();
  TextEditingController stateController = TextEditingController();
  TextEditingController emerContactController = TextEditingController();
  List<String> _genders = <String>['', 'Male', 'Female', 'Others'];
  String gender = '';
  DateTime selectedDate = DateTime.now();
  List<String> _states = <String>[
    '',
    'AndhraPradesh(AP)',
    'ArunachalPradesh(AR)',
    'Assam(AS)',
    'Bihar(BR)',
    'Chhattisgarh(CG)',
    'Goa(GA)',
    'Gujarat(GJ)',
    'Haryana(HR)',
    'HimachalPradesh(HP)',
    'Jammu&Kashmir(JK)',
    'Jharkhand(JH)',
    'Karnataka(KA)',
    'Kerala(KL)',
    'MadhyaPradesh(MP)',
    'Maharashtra(MH)',
    'Manipur(MN)',
    'Meghalaya(ML)',
    'Mizoram(MZ)',
    'Nagaland(NL)',
    'Orissa(OR)',
    'Punjab(PB)',
    'Rajasthan(RJ)',
    'Sikkim(SK)',
    'TamilNadu(TN)',
    'Tripura(TR)',
    'Telangana(TL)',
    'UttarPradesh(UP)',
    'Uttarakhand(UK)',
    'WestBengal(WB)'
  ];
  String states = '';
  TextEditingController popupController = TextEditingController();

  createAlertDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Enter OTP"),
            content: TextField(
              controller: popupController,
            ),
            actions: <Widget>[
              MaterialButton(
                elevation: 5.0,
                child: Text(
                  'Submit',
                  style: TextStyle(color: Colors.blue),
                ),
                onPressed: () {
                  print("otp : " + popupController.text);
                  Navigator.of(context).pop(popupController.text.toString());
                  Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new SuccessPage()),
                  );
                },
              ),
              MaterialButton(
                elevation: 5.0,
                child: Text(
                  'Resend',
                  style: TextStyle(color: Colors.blue),
                ),
                onPressed: () {},
              ),
            ],
          );
        });
  }

  final TextEditingController dateController = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formkey,
      autovalidate: true,
      child: Padding(
          padding: const EdgeInsets.all(0.0),
          child: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Colors.deepOrange[200],
                      Colors.white10,
                      Colors.lightGreen,
                    ]),
              ),
              child: ListView(
                children: <Widget>[
                  SizedBox(
                    height: 50.0,
                    child: Container(
                      padding: new EdgeInsets.all(10.0),
                      child: Center(
                        child: Text(
                          'COVID -19',
                          style: TextStyle(
                              fontSize: 30.0,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 50.0,
                    child: Container(
                      child: Center(
                        child: Text(
                          'Register Here !!!',
                          style: TextStyle(
                              fontSize: 20.0,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: new EdgeInsets.only(
                        top: 10.0, bottom: 10.0, left: 40.0, right: 40.0),
                    child: TextFormField(
                      controller: nameController,
                      validator: (value) =>
                          value.isEmpty ? 'Name is required' : null,
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.person),
                        hintText: 'Enter Your First and Last Name',
                        labelText: 'Name',
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50.0),
                          borderSide: BorderSide(color: Colors.black),
                        ),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(50.0)),
                      ),
                      keyboardType: TextInputType.text,
                      inputFormatters: [
                        new LengthLimitingTextInputFormatter(20)
                      ],
                    ),
                  ),
                  Padding(
                    padding: new EdgeInsets.only(
                        top: 10.0, bottom: 10.0, left: 40.0, right: 40.0),
                    child: TextFormField(
                      controller: mobileController,
                      maxLength: 10,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Enter your mobile number';
                        }
                        if (value.length < 10) {
                          return 'Mobile number should be minimum 10 digits';
                        }
                      },
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.phone),
                        hintText: 'Enter Your Mobile Number',
                        labelText: 'Mobile Number',
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50.0),
                          borderSide: BorderSide(color: Colors.black),
                        ),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(50.0)),
                      ),
                      inputFormatters: [
                        WhitelistingTextInputFormatter.digitsOnly,
                      ],
                    ),
                  ),
                  Padding(
                    padding: new EdgeInsets.only(
                        bottom: 5.0, left: 40.0, right: 40.0),
                    child: TextFormField(
                      controller: emerContactController,
                      maxLength: 10,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Enter your emergency number';
                        }
                        if (value.length < 10) {
                          return 'Mobile number should be minimum 10 digits';
                        }
                      },
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.phone),
                        hintText: 'Enter Your Emergency Number',
                        labelText: 'Emergency Number',
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50.0),
                          borderSide: BorderSide(color: Colors.black),
                        ),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(50.0)),
                      ),
                      inputFormatters: [
                        WhitelistingTextInputFormatter.digitsOnly,
                      ],
                    ),
                  ),
                  Padding(
                    padding: new EdgeInsets.only(
                        bottom: 5.0, left: 40.0, right: 40.0),
                    child: TextFormField(
                      controller: dateController,
                      validator: (value) =>
                          value.isEmpty ? 'DOB is required' : null,
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.calendar_today),
                        hintText: 'yyyy/mm/dd',
                        labelText: 'Date Of Birth',
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50.0),
                          borderSide: BorderSide(color: Colors.black),
                        ),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(50.0)),
                      ),
                      keyboardType: TextInputType.datetime,
                      inputFormatters: [
                        WhitelistingTextInputFormatter.digitsOnly,
                      ],
                      onTap: () async {
                        DateTime date = DateTime(1900);
                        date = await showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(1900),
                            lastDate: DateTime(2100));
                        dateController.text = date.toString().substring(0, 10);
                      },
                    ),
                  ),
                  Padding(
                    padding: new EdgeInsets.only(
                        top: 15.0, bottom: 10.0, left: 40.0, right: 40.0),
                    child: FormField(
                      builder: (FormFieldState state) {
                        return InputDecorator(
                          decoration: InputDecoration(
                            prefixIcon: Icon(Icons.group),
                            labelText: 'Gender',
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(40.0),
                              borderSide: BorderSide(color: Colors.black),
                            ),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(40.0)),
                          ),
                          isEmpty: gender == '',
                          child: new DropdownButtonHideUnderline(
                            child: new DropdownButton(
                              value: gender,
                              isDense: true,
                              onChanged: (String newValue) {
                                setState(() {
                                  gender = newValue;
                                  genderController.text = gender;
                                });
                              },
                              items: _genders.map((String value) {
                                return new DropdownMenuItem(
                                  value: value,
                                  child: new Text(value),
                                );
                              }).toList(),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: new EdgeInsets.only(
                        top: 10.0, bottom: 10.0, left: 40.0, right: 40.0),
                    child: FormField(
                      validator: (states) {
                        if (states == '') {
                          return 'Enter your states';
                        }
                      },
                      builder: (FormFieldState state) {
                        return InputDecorator(
                          decoration: InputDecoration(
                            prefixIcon: Icon(Icons.location_city),
                            labelText: 'State',
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(50.0),
                              borderSide: BorderSide(color: Colors.black),
                            ),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(50.0)),
                          ),
                          isEmpty: states == '',
                          child: new DropdownButtonHideUnderline(
                            child: new DropdownButton(
                              value: states,
                              isDense: true,
                              onChanged: (String newValue) {
                                setState(() {
                                  states = newValue;
                                  stateController.text = states;
                                });
                              },
                              items: _states.map((String value) {
                                return new DropdownMenuItem(
                                  value: value,
                                  child: new Text(value),
                                );
                              }).toList(),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: new EdgeInsets.only(top: 2.0, bottom: 2.0),
                  ),
                  new Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.all(10),
                          child: RaisedButton(
                            padding: const EdgeInsets.all(10.0),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0)),
                            color: Colors.green[600],
                            splashColor: Colors.yellow[200],
                            child: Text(
                              'Register',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20.0),
                            ),
                            onPressed: () {
                              submitForm();
                            },
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.all(10),
                          child: RaisedButton(
                            padding: const EdgeInsets.all(10.0),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0)),
                            color: Colors.green[600],
                            splashColor: Colors.yellow[200],
                            child: Text(
                              'Reset',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20.0),
                            ),
                            onPressed: () {
                              resetForm();
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ))),
    );
  }

  extractStateCode() {
    var statCode = stateController.text;
    int start = statCode.indexOf("(") + 1;
    int end = statCode.indexOf(")");
    stateController.text = statCode.substring(start, end).toString();
  }

  submitForm() {
    if (_formkey.currentState.validate()) {
      _formkey.currentState.save();
      // var convertedDate = dateController.text
      //     .toString()
      //     .substring(2, 7)
      //     .replaceAll('-', '')
      //     .split(' ')[0]
      //     .trim();
      extractStateCode();
      var user = new User(
          nameController.text,
          mobileController.text,
          emerContactController.text,
          dateController.text,
          genderController.text,
          stateController.text);
      print(user.toString());
      createAlertDialog(context);
    }
  }

  resetForm() {
    setState(() {
      nameController.clear();
      mobileController.clear();
      emerContactController.clear();
      dateController.clear();
      genderController.clear();
      stateController.clear();
    });
  }
  // nameController.text = '';
  // mobileController.text = '';
  // emerContactController.text = '';
  // dateController.text = '';
  // genderController.text = ' ';
  // stateController.text = ' ';
}
