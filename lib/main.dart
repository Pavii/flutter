import 'package:flutter/material.dart';
import 'screens/registrationPage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTitle = 'IndiaPrevents';
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: appTitle,
      home: Scaffold(
          appBar: AppBar(
            title: Text(appTitle, style: TextStyle(color: Colors.white,fontSize: 25.0)),
            backgroundColor: Colors.deepOrangeAccent[200],
            centerTitle: true,
          ),
          body: new Container(
            child: RegistrationPage(),
          )),
    );
  }
}