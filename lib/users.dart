class User {
  String name;
  String dob;
  String mobile;
  String emMobile;
  String state;
  String gender;

  User(
    String name,
    String mobile,
    String emMobile,
    String dob,
    String gender,
    String state,
  ) {
    this.name = name;
    this.mobile = mobile;
    this.emMobile = emMobile;
    this.dob = dob;
    this.gender = gender;
    this.state = state;
  }

  String get emPhoneNum {
    return emMobile;
  }

  set emPhoneNum(String emMobile) {
    this.emMobile = emMobile;
  }

  String get states {
    return state;
  }

  set states(String state) {
    this.state = state;
  }

  String get username {
    return name;
  }

  set username(String name) {
    this.name = name;
  }

  String get dateOfBirth {
    return dob;
  }

  set dateOfBirth(String dob) {
    this.dob = dob;
  }

  String get phoneNum {
    return mobile;
  }

  set phoneNum(String mobile) {
    this.mobile = mobile;
  }

  String get userGender {
    return gender;
  }

  set userGender(String gender) {
    this.gender = gender;
  }

  String toString() {
    return "UserName : " +
        username +
        "," +
        "Mobile : " +
        mobile +
        "," +
        "Emergency Mobile : " +
        emMobile +
        "," +
        "Gender : " +
        gender +
        "," 
        "State : " +
        state + ","
        "Date Of Birth : " +
        dob +
        "," +
        " ";
  }
}
